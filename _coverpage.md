<!-- _coverpage.md -->

![logo](_images/LogoWhiteFooter.png)

# <span style="color:white">Quantico <small>DOCS</small></span>

> <span style="color:white">A Knowledge Base for all products that includes:</span>

<span style="color:white">- Presentation Doc</span><br>
<span style="color:white">- PRD (Product Requirements Doc)</span><br>
<span style="color:white">- Tech specs</span><br>
<span style="color:white">- QA Docs</span><br>


[GitLab](https://qgitlab.quanticotrends.com/quanticoteam/main/doc-technical)

<!-- background color -->

![color](#460d97)