## Presentation
This repository is dedicated to centralize all the documentation from every product developed by the Quantico Tech team.
Every product is contained in 1 folder, each one with 4 segments: README, PRD (Product Requirement Document), Tech Specs and QA Docs.
These formats are supported:
- iframe .html, .htm
- markdown .markdown, .md
- audio .mp3
- video .mp4, .ogg
- code other file extension

## Prerequisites
The desktop application supports these operating systems:
- macOS 10.9 and above (64-bit only)
- Linux Ubuntu 12.04 and above, Fedora 21 and Debian 8 (64-bit only)
- Windows 7 and above

## Get started
1. Install Docsify
~~~
    npm install docsify
~~~
2. Install Docker
~~~
    RUN npm install -g docsify-cli@latest
~~~
3. Clone the repo
4. Create a new folder by copying the example or edit an existing one
5. Push changes directly into master
6. To visualize it in your local host
~~~
    docsify serve
~~~

For more information [go to Docsify docs](https://docsify.js.org/).

## Deployment
To deploy on Gitlab pages
If you are deploying your master branch, include .gitlab-ci.yml with the following script:

The .public workaround is so cp doesn't also copy public/ to itself in an infinite loop.

~~~
pages:
  stage: deploy
  script:
  - mkdir .public
  - cp -r * .public
  - mv .public public
  artifacts:
    paths:
    - public
  only:
  - master
~~~

You can replace script with - cp -r docs/. public, if ./docs is your Docsify subfolder.

## Public access URL:

[https://lkina.gitlab.io/docs-example](https://lkina.gitlab.io/docs-example)
