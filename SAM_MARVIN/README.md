# SAM MARVIN APP

## Presentation

This app automates the analysis and categorization of texts, from uploading excel documents.

## Context

It was originally thought for a company that carries out several customer satisfaction surveys. And while there was an initial classification from the survey tool, that only sorted the feedback in three groups ("detractor", "pasivo", "promotor"). 
Since a deeper analysis was needed in order to tag each comment accurately, and so far this has been a manual process, the necessity to have a tool that could automate the process was created.

## Core concepts (Dictionary)

- Modelo: It refers to the base data with which the AI ​​is trained.
- Proceso: A process refers to the action of analyzing a database (excel or csv document)
- Benchmark: Comparative charts of tags performance on a period of time, it is obtained by comparing two or more "procesos".
- Etiquetas: Tags that the process assigns to each text or comment.

## Prerequisites

- The desktop application supports these operating systems:
    - macOS 10.9 and above (64-bit only)
    - Linux Ubuntu 12.04 and above, Fedora 21 and Debian 8 (64-bit only)
    - Windows 7 and above
To access the app you must have an account created with a user and password. This account could be requested by anyone on the commercial team via mail to soporte@quanticotrends.com.
The account must be associated with at least 1 model. If a new model is required, contact comercial@quanticotrends.com.

## Get started

As a user, access the app http://sam.marvin.pe/signin and follow the instructions in the End user manual
As a developer, access the repo https://qgitlab.quanticotrends.com/quanticoteam/marvin/www-marvin find more info in the section tech specs or read the instructions for deployment, monitoring or maintenance.

## Product Version

SAM MARVIN APP v2.0

## Changelog

| Version             | Release date | Relevant changes                                            |
|---------------------|--------------|-------------------------------------------------------------|
| SAM MARVIN APP v1.0 | 20/01/2021   | 1ra versión, origen                                         |
| SAM MARVIN APP v2.0 | 15/02/2021   | Refactorización del app y mejoras al modelo de calificación |

## Release notes, news (opcional)

| Version             | Estimated release date | Relevant changes                            |
|---------------------|------------------------|---------------------------------------------|
| SAM MARVIN APP v2.1 | 20/05/2021             | Features:<br>- feature one<br>- feature two |
| SAM MARVIN APP v3.0 | 15/07/2021             |  Bugs:<br>- big bug<br>- little bug         |

